import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-editauthor',
  templateUrl: './editauthor.component.html',
  styleUrls: ['./editauthor.component.css']
})
export class EditauthorComponent implements OnInit {
  id:number;
  authorInput:string;

  constructor(
    private router:Router,
    private activatedroute:ActivatedRoute) { }

  ngOnInit() {
    this.authorInput = this.activatedroute.snapshot.params['authorName'];
    this.id = this.activatedroute.snapshot.params['id'];
  }
  onSubmit(){
    this.router.navigate(['/authors', this.id,this.authorInput])
  }

}
