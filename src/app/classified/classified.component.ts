import { ArticlesService } from './../articles.service';
import { Component, OnInit } from '@angular/core';
import { ClassifyService } from '../classify.service';
import { ImageService } from '../image.service';
import { stringify } from 'querystring';

@Component({
  selector: 'app-classified',
  templateUrl: './classified.component.html',
  styleUrls: ['./classified.component.css']
})
export class ClassifiedComponent implements OnInit {

  category:string = "Loading...";
  categoryImage:string;
  categorytypes:object[]=[{name: 'business'}, {name: 'entertainment'}, {name: 'politics'}, {name: 'sport'}, {name: 'tech'}];
  body: string;
  aicat: string;
  mancat: string;
  
  constructor(public classifyservice:ClassifyService,
              public imageservice:ImageService,
              public articlesservice:ArticlesService) { }


              ngOnInit() {
                this.classifyservice.classify().subscribe(
                  res => {
                    this.category = this.classifyservice.categories[res];
                    this.categoryImage = this.imageservice.images[res];
                    this.body = this.classifyservice.doc;
                  }
                )
              }

              onSubmit(){
                this.articlesservice.saveArticle(this.body,this.category,this.mancat)
              }
            

}
