import { AuthService } from './../auth.service';
import { forkJoin, Observable } from 'rxjs';
import { Users } from './../interfaces/users';
import { PostsService } from './../posts.service';
import { Component, OnInit } from '@angular/core';
import { Posts } from '../interfaces/posts';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
  users$: Users[];
  //posts$: Posts[];
  title:String;
  body:String;
  author:String;
  success:boolean =false;
  pushAlert:string;
  userId:string;
  posts$:Observable<any>;
  constructor(private postservice:PostsService,
              public authservice:AuthService) { }

  ngOnInit() {
    //forkJoin(this.postservice.getPosts(),this.postservice.getUsers()).subscribe(
      //([data1,data2])=>
      //{
       // this.posts$=data1,
     //this.users$=data2
     this.authservice.user.subscribe(
       user=>{
         this.userId=user.uid;
         this.posts$=this.postservice.getPosts(this.userId);
        }
     )
     
    }
    deletePost(id:string){
      this.postservice.deletePost(id,this.userId);
    }
  
  
 /* onSubmit(){
    for(let i=0; i<this.posts$.length;i++){
      for(let j=0; j<this.users$.length;j++){
        if(this.posts$[i].userId==this.users$[j].id){
          this.title = this.posts$[i].title;
          this.body = this.posts$[i].body;
          this.author= this.users$[j].name;
          this.postservice.savePosts(this.title,this.body,this.author)
        }
        this.success=true;
        this.pushAlert= "posts added";
      }
    }
  }*/

}
