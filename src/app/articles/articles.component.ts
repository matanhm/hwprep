import { AuthService } from './../auth.service';
import { Observable } from 'rxjs';
import { ArticlesService } from './../articles.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-articles',
  templateUrl: './articles.component.html',
  styleUrls: ['./articles.component.css']
})
export class ArticlesComponent implements OnInit {
articles$:Observable<any>

  constructor(public articleservice:ArticlesService,
              public authservice:AuthService) { }

  ngOnInit() {
    this.articles$=this.articleservice.getArticles()

  }

}
