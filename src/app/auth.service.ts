import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFireAuth } from '@angular/fire/auth';
import { User } from './interfaces/user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  user: Observable<User | null>
  err: any;
  constructor(public afAuth:AngularFireAuth,
                private router:Router) { 
    this.user = this.afAuth.authState;
  }

  signUp(email:string , password:string){
    this.afAuth
      .auth
      .createUserWithEmailAndPassword(email,password)
      .then(user => {
        this.router.navigate (['/welcome'])
      }).catch (
        error => this.err = error
         ) 

  }
  logout(){
    this.afAuth.auth.signOut().then(user=>{this.router.navigate(['/welcome']);
  })
  }
  login(email:string, password:string){
    this.afAuth.auth.signInWithEmailAndPassword(email,password)
    .then(user=>{this.router.navigate(['/welcome']);
  }).catch (
    error => this.err = error
     ) 
    }

  

}


