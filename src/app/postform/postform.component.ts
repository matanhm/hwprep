import { AuthorsService } from './../authors.service';
import { Router, ActivatedRoute } from '@angular/router';
import { PostsService } from './../posts.service';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-postform',
  templateUrl: './postform.component.html',
  styleUrls: ['./postform.component.css']
})
export class PostformComponent implements OnInit {
  title: String;
  body: String;
  author: String;
  id:string;
  isEdit:boolean=false;
  buttonText:string= "Add Book"
  userId:string;

  constructor(private postsservice:PostsService,
              private router:Router,
              private route:ActivatedRoute,
              public  authservice:AuthService) { }

  ngOnInit() {
    this.id = this.route.snapshot.params.id; 
    this.authservice.user.subscribe(
      user=>{
        this.userId = user.uid;
      }
    )
    console.log(this.id);

    if(this.id){
      this.isEdit = true;
      this.buttonText= "Update Book";
      this.postsservice.getPost(this.id, this.userId).subscribe(
        post=> {
          this.title =post.data().title;
          this.body = post.data().body;
          this.author= post.data().author;
        }
      )
    }

  }
  onSubmit(){
    if(this.isEdit){
    this.postsservice.updatePost(this.userId,this.id,this.title,this.body,this.author)
    }
    else{
      this.postsservice.addPost(this.userId,this.title,this.body,this.author)
    }
    this.router.navigate(['/posts'])
  }


}
