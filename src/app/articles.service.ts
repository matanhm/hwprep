import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ArticlesService {

  constructor(private db:AngularFirestore) { }

  saveArticle(body:string, aicat:string, mancat:string){
    const article= {body:body, aicat:aicat, mancat:mancat}
    this.db.collection('articles').add(article)
  }
  getArticles(): Observable<any>{
    return this.db.collection('articles').valueChanges(({idField:'id'}));      
    }

}
