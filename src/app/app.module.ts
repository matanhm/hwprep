import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MatSliderModule } from '@angular/material/slider';
import {MatExpansionModule} from '@angular/material/expansion';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavComponent } from './nav/nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { BooksComponent } from './books/books.component';
import { AuthorsComponent } from './authors/authors.component';
import { RouterModule, Routes } from '@angular/router';
import { EditauthorComponent } from './editauthor/editauthor.component';
import {FormsModule} from '@angular/forms';
import {MatCardModule} from '@angular/material/card';
import { PostsComponent } from './posts/posts.component';
import { HttpClientModule } from '@angular/common/http';
import { AngularFireModule } from '@angular/fire';
import { environment } from '../environments/environment';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { PostformComponent } from './postform/postform.component';
import { AngularFireAuth } from '@angular/fire/auth';
import { SignupComponent } from './signup/signup.component';
import { LoginComponent } from './login/login.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material';
import {MatInputModule} from '@angular/material';
import { WelcomeComponent } from './welcome/welcome.component';
import { DocformComponent } from './docform/docform.component';
import { ClassifiedComponent } from './classified/classified.component';
import { ArticlesComponent } from './articles/articles.component';


const appRoutes: Routes = [
  { path: 'books', component: BooksComponent },
  { path: 'authors/:id/:authorName', component: AuthorsComponent },
  { path: 'editauthor/:id/:authorName', component: EditauthorComponent },
  { path: 'posts', component: PostsComponent },
  { path: 'postform', component: PostformComponent },
  { path: 'postform/:id', component: PostformComponent },
  { path: 'signup', component: SignupComponent },
  { path: 'login', component: LoginComponent },
  { path: 'welcome', component: WelcomeComponent },
  { path: 'docform', component: DocformComponent },
  { path: 'classified', component: ClassifiedComponent },
  { path: 'articles', component: ArticlesComponent },
  { path: "",
    redirectTo: '/books',
    pathMatch: 'full'
  },
];

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    BooksComponent,
    AuthorsComponent,
    EditauthorComponent,
    PostsComponent,
    PostformComponent,
    SignupComponent,
    LoginComponent,
    WelcomeComponent,
    DocformComponent,
    ClassifiedComponent,
    ArticlesComponent
  ],
  imports: [
    AngularFireAuthModule ,
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireStorageModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    ),
    BrowserModule,
    BrowserAnimationsModule,
    MatSliderModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatExpansionModule,
    FormsModule,
    MatCardModule,
    HttpClientModule,
    AngularFireModule,
    AngularFireAuthModule,
    AngularFireStorageModule,
    AngularFirestoreModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule
  ],
  providers: [AngularFireAuth ],
  bootstrap: [AppComponent]
})
export class AppModule { }
