import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthorsService {
 authors:any [] =[{name:'Lewis Carrol', id:1},{name:'Leo Tolstoy' ,id:2},{name:'Thomas Mann',id:3}]

 getAuthors(){
  const authorsObservable = new Observable(
    observer => {
      setInterval(
        () => observer.next(this.authors),4000
      )
    }
  )
  return authorsObservable;
 }  
 addAuthor(newAuthor:string){
  this.authors.push({name:newAuthor});
}


  constructor() { }
}
