import { AuthorsService } from './../authors.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-authors',
  templateUrl: './authors.component.html',
  styleUrls: ['./authors.component.css']
})
export class AuthorsComponent implements OnInit {
  authors$:Observable<any>;
newAuthor:string;
authorName:string;
id:number;
constructor(private activatedroute:ActivatedRoute,
            private authorsservice:AuthorsService ) { }

  ngOnInit() {
    //this.id = this.activatedroute.snapshot.params['id'];
    //this.authorName = this.activatedroute.snapshot.params['authorName'];
    this.authors$ = this.authorsservice.getAuthors();
  
  }
  onSubmit(){
    this.authorsservice.addAuthor(this.newAuthor);
  }

}
