import { Users } from './interfaces/users';
import { Posts } from './interfaces/posts';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable} from 'rxjs';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PostsService {
  userCollection: AngularFirestoreCollection = this.db.collection('users');
  postCollection: AngularFirestoreCollection;
   urlPosts = "https://jsonplaceholder.typicode.com/posts/";
   urlUsers=  "https://jsonplaceholder.typicode.com/users";
  constructor(private http:HttpClient ,
              private db:AngularFirestore) { }

  getPosts(userId:string):Observable<any[]>{
   // return this.http.get<Posts[]>(this.urlPosts)
     // return this.db.collection('posts').valueChanges({idField:'id'});
     this.postCollection = this.db.collection(`users/${userId}/posts`);
     return this.postCollection.snapshotChanges().pipe(
       map(
         collection => collection.map(
           document => {
             const data = document.payload.doc.data();
             data.id = document.payload.doc.id;
             return data;
           }
         )
       )
     )
  }

  getUsers(){
    return this.http.get<Users[]>(this.urlUsers)
  }
  savePosts(title:String, body:String, author:String){
    const post= {title:title, body:body, author:author}
    this.db.collection('posts').add(post)
  }
  addPost(userId:string,title:String, author:String, body:String){
    const post= {title:title, author:author, body:body}
   // this.db.collection('posts').add(post);
      this.userCollection.doc(userId).collection('posts').add(post)
  }
  deletePost(id:string,userId:string){
    this.db.doc(`users/${userId}/posts/${id}`).delete();
  }
  updatePost(userId:string, id:String, title:String, author:String, body:String){
    this.db.doc(`users/${userId}/posts/${id}`).update({
      title:title,
      body:body,
      author:author
    })
  }
    getPost(id:String, userId:string):Observable<any>{
      return this.db.doc(`users/${userId}/posts/${id}`).get();
    }
  

}
