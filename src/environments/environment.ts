// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
    // Your web app's Firebase configuration
     firebaseConfig : {
      apiKey: "AIzaSyC4t4AT5vVD78jpkqO7wzkSFgHsJq1cUH0",
      authDomain: "hwprep-67199.firebaseapp.com",
      databaseURL: "https://hwprep-67199.firebaseio.com",
      projectId: "hwprep-67199",
      storageBucket: "hwprep-67199.appspot.com",
      messagingSenderId: "698282547079",
      appId: "1:698282547079:web:5285457be9987264d8e522"
    }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
